package com.example.clip.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.example.clip.model.Payment;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
public class TranscationControllerTestIntegration {

	@Autowired
	private MockMvc mvc;

	@Test
	public void createPayment() throws Exception {
		mvc.perform(post("/transaction/").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(new Payment(new BigDecimal(44.5), "123"))).accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isCreated()).andExpect(jsonPath("$.userId").value("123"))
				.andExpect(jsonPath("$.amount").value(44.5));
	}

	@Test
	public void getUsers() throws Exception {
		mvc.perform(get("/transaction/users").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.content").exists());
	}

	@Test
	public void validatePaginate() throws Exception {
		mvc.perform(get("/transaction/users?page=0&size=1").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.numberOfElements").value(1));
	}

	@Test
	public void callDisbursementProcess() throws Exception {
		mvc.perform(get("/transaction/disbursement/run").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	public void report() throws Exception {
		mvc.perform(get("/transaction/report").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
