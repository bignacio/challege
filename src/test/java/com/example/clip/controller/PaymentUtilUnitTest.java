package com.example.clip.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import com.example.clip.model.Payment;
import com.example.clip.util.enums.PaymentUtil;

public class PaymentUtilUnitTest {
	
	@Test
	public void calculateDisbursement() throws Exception {
		Payment payment = new Payment(new BigDecimal(100.00), "123");
		BigDecimal disbursement =  PaymentUtil.calculateDisbursement(payment);
		assertEquals(disbursement, new BigDecimal(96.50).setScale(2));
	}

}
