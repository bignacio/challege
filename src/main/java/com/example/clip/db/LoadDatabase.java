package com.example.clip.db;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.clip.model.Payment;
import com.example.clip.model.PaymentStatus;
import com.example.clip.model.User;
import com.example.clip.repository.PaymentRepository;
import com.example.clip.repository.PaymentStatusRepository;
import com.example.clip.repository.UserRepository;

@Configuration
public class LoadDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);
	
	@Autowired
	BCryptPasswordEncoder bcrypt;

	
	@Bean
	CommandLineRunner addPaymentStatus(PaymentStatusRepository repository) {
		return args -> {
			repository.save(new PaymentStatus(1, "NEW"));
			repository.save(new PaymentStatus(2, "PROCESSED"));
		};
	}
	
	@Bean
	CommandLineRunner initDatabase(PaymentRepository repository) {
		return args -> {
			log.info("Preloading database... ");
			List<Payment> payments;
			final int MAX_PAYMENTS = 10000;
			int minUserId = 1000;
			int maxUserId = 1500;
			int minAmount = 20;
			int maxAmount = 2000000;
			payments = new ArrayList<Payment>();
			for (int user = 0; user < MAX_PAYMENTS; user++) {
				String userId = Integer.toString(ThreadLocalRandom.current().nextInt(minUserId, maxUserId));
				BigDecimal amount = new BigDecimal(ThreadLocalRandom.current().nextLong(minAmount, maxAmount));
				payments.add(new Payment(amount, userId));
			}
			repository.saveAll(payments);
		};
	}
	
	@Bean
	CommandLineRunner addAdminUser(UserRepository repository) {
		return args -> {
			repository.save(new User("admin", bcrypt.encode("12345"), "admin@clip.mx"));
			log.info("finished");
		};
	}


}
