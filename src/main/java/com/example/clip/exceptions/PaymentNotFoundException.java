package com.example.clip.exceptions;

public class PaymentNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public PaymentNotFoundException(long id) {
		super("Could not find payment with id: " + id);
	}

}
