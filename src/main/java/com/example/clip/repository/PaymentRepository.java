package com.example.clip.repository;

import com.example.clip.interfaces.UserIdOnly;
import com.example.clip.model.Payment;
import com.example.clip.model.PaymentStatus;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PaymentRepository extends JpaRepository<Payment, Long> {
	
	 Page<UserIdOnly> findDistinctBy(Pageable pageable);
	 List<Payment> findByPaymentStatus(PaymentStatus paymentStatus);

}
