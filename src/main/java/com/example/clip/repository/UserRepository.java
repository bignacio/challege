package com.example.clip.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.clip.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}