package com.example.clip.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;


@Getter
@Setter
@AllArgsConstructor
public class PaymentRequest {

	@NotEmpty
    String userId;
	
	@Digits(integer = 6, fraction=2)
    BigDecimal amount;
}
