package com.example.clip.request;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReportByUser {
	private String userId;
	private BigDecimal payments;
	private BigDecimal newPayments;
	private BigDecimal processedPayments;

}
