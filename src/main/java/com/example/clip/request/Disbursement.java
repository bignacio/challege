package com.example.clip.request;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Disbursement {

	private String userId;
	private BigDecimal payment;
	private BigDecimal disbursement;
}
