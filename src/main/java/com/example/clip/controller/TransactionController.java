package com.example.clip.controller;

import com.example.clip.interfaces.UserIdOnly;
import com.example.clip.model.Payment;
import com.example.clip.request.Disbursement;
import com.example.clip.request.PaymentRequest;
import com.example.clip.request.ReportByUser;
import com.example.clip.service.PaymentService;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/transaction")
public class TransactionController {

	@Autowired
	PaymentService paymentService;

	@PostMapping("/")
	public  ResponseEntity<EntityModel<Payment>> create(@RequestBody @Valid PaymentRequest paymentRequest) {
		return paymentService.create(paymentRequest);
	}
	
	@GetMapping("/{id}")
	public  ResponseEntity<EntityModel<Payment>> one(@PathVariable("id") long id) {
		return paymentService.one(id);
	}
   
	@GetMapping("/users")
	public Page<UserIdOnly> usersHasPayments(Pageable pageable) {
		return paymentService.findUsersHasPayments(pageable);
	}
	
	@GetMapping("/disbursement/run")
	public ResponseEntity<List<Disbursement>> disbursementProcess() {
		return paymentService.disbursementProcess();
	}
	
	@GetMapping("/report")
	public ResponseEntity<List<ReportByUser>> getReportPerUser() {
		return paymentService.getReportByUser();
	}

}
