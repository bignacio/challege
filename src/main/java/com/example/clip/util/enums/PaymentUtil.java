package com.example.clip.util.enums;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.example.clip.model.Payment;

public class PaymentUtil {

	public static final long NEW = 1;
	public static final long PROCESSED = 2;
	public static final BigDecimal FEE = new BigDecimal(0.035);

	public static BigDecimal calculateDisbursement(Payment payment) {
		BigDecimal disbursement = payment.getAmount().subtract(payment.getAmount().multiply(PaymentUtil.FEE));

		return disbursement.setScale(2, RoundingMode.CEILING);
	}

	public static BigDecimal calculateTotal(List<Payment> payments) {
		return payments.stream().map(payment -> payment.getAmount()).reduce(BigDecimal.ZERO,
				(total, amount) -> total.add(amount));
	}

	public static BigDecimal calculatePaymentsTotalByStatus(List<Payment> payments, long statusId) {
		return payments.stream().filter(payment -> payment.getPaymentStatus().getId() == statusId)
				.map(payment -> payment.getAmount()).reduce(BigDecimal.ZERO, (total, amount) -> total.add(amount));
	}

}
