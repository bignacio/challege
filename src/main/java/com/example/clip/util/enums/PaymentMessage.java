package com.example.clip.util.enums;

public class PaymentMessage {

	public static final String DISBURSEMENT_PROCESS_LOG = "Running disbursement process";
	public static final String REPORT_BY_USER_LOG = "Generating report by user";
	public static final String USER_HAS_PAYMENTS_LOG = "Getting all users has transactions";
	public static final String GET_ONE_LOG = "Getting one payment";
	public static final String CREATE_LOG = "Payment created successfully";
}
