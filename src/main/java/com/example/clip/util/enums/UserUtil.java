package com.example.clip.util.enums;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


public class UserUtil {
	
	@Autowired
	static BCryptPasswordEncoder bcrypt;
	
	
	public static String encryptPassword(String password) {
		return bcrypt.encode(password);
	}
	
	

}
