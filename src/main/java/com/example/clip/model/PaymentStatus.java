package com.example.clip.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="payment_status")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PaymentStatus {

	@Id
	@Column(name = "payst_id")
	private long id;
	
	@Column(name = "payst_status")
	private String status;
	
	public PaymentStatus(long id) {
		this.id = id;
	}
	
}
