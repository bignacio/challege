package com.example.clip.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.clip.util.enums.PaymentUtil;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "payment")
@AllArgsConstructor
@Getter
@Setter
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "payment_id")
    private long id;

    @Column(name = "payment_amount")
    private BigDecimal amount;

    @Column(name = "payment_user_id")
    private String userId;
    
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private PaymentStatus paymentStatus;
    
    public Payment(BigDecimal amount, String userId) {
    	this.amount = amount;
    	this.userId = userId;
    	this.paymentStatus = new PaymentStatus(PaymentUtil.NEW);
    }
    
    public Payment() {
    	this.paymentStatus = new PaymentStatus(PaymentUtil.NEW);
    }

}
