package com.example.clip.interfaces;

public interface UserIdOnly {
	String getUserId();
}
