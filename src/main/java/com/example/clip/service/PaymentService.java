package com.example.clip.service;

import javax.persistence.PersistenceException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.clip.controller.TransactionController;
import com.example.clip.exceptions.PaymentNotFoundException;
import com.example.clip.interfaces.UserIdOnly;
import com.example.clip.model.Payment;
import com.example.clip.model.PaymentStatus;
import com.example.clip.repository.PaymentRepository;
import com.example.clip.request.Disbursement;
import com.example.clip.request.PaymentRequest;
import com.example.clip.request.ReportByUser;
import com.example.clip.util.enums.PaymentMessage;
import com.example.clip.util.enums.PaymentUtil;

@Service
public class PaymentService extends SuperService {

	@Autowired
	PaymentRepository paymentRepository;

	/**
	 * Creates a new payment transaction
	 * 
	 * @param paymentRequest
	 * @return The new user created
	 */
	public ResponseEntity<EntityModel<Payment>> create(PaymentRequest paymentRequest) {
		Payment payment = new Payment();
		payment.setAmount(paymentRequest.getAmount());
		payment.setUserId(paymentRequest.getUserId());

		try {
			Payment paymentSaved = paymentRepository.save(payment);
			log.info(PaymentMessage.CREATE_LOG);

			return new ResponseEntity<EntityModel<Payment>>(
					EntityModel.of(paymentSaved,
							linkTo(methodOn(TransactionController.class).one(paymentSaved.getId())).withSelfRel()),
					HttpStatus.CREATED);
		} catch (PersistenceException e) {
			log.error(e.getMessage());
			return ResponseEntity.internalServerError().build();
		}

	}

	/**
	 * Finds a payment with id given
	 * 
	 * @param id
	 * @return The found payment or PaymentNotFoundException
	 */
	public ResponseEntity<EntityModel<Payment>> one(long id) {
		log.info(PaymentMessage.GET_ONE_LOG);
		Payment paymentFound = this.paymentRepository.findById(id).orElseThrow(() -> new PaymentNotFoundException(id));
		return ResponseEntity.ok(EntityModel.of(paymentFound,
				linkTo(methodOn(TransactionController.class).one(paymentFound.getId())).withSelfRel()));
	}

	/**
	 * Finds all users with payments
	 * 
	 * @param pageable
	 * @return A list of users with payments
	 */
	public Page<UserIdOnly> findUsersHasPayments(Pageable pageable) {
		log.info(PaymentMessage.USER_HAS_PAYMENTS_LOG);
		return this.paymentRepository.findDistinctBy(pageable);
	}

	/**
	 * Runs a disbursement process
	 * 
	 * @return A list of all transactions with disbursement
	 */
	public ResponseEntity<List<Disbursement>> disbursementProcess() {
		log.info(PaymentMessage.DISBURSEMENT_PROCESS_LOG);
		List<Payment> paymentsForProcess = this.paymentRepository
				.findByPaymentStatus(new PaymentStatus(PaymentUtil.NEW));

		if (paymentsForProcess.isEmpty() == false) {
			List<Disbursement> paymentWithDisbursement = new ArrayList<>();
			List<Payment> paymetsForUpdate = paymentsForProcess.stream().map(payment -> {
				payment.setPaymentStatus(new PaymentStatus(PaymentUtil.PROCESSED));
				BigDecimal disbursement = PaymentUtil.calculateDisbursement(payment);
				paymentWithDisbursement.add(new Disbursement(payment.getUserId(), payment.getAmount(), disbursement));

				return payment;
			}).collect(Collectors.toList());

			this.paymentRepository.saveAll(paymetsForUpdate);

			return ResponseEntity.ok(paymentWithDisbursement);
		}

		return ResponseEntity.ok(new ArrayList<Disbursement>());
	}

	/**
	 * Build a report with transactions by users
	 * @return A list of reports by user
	 */
	public ResponseEntity<List<ReportByUser>> getReportByUser() {
		log.info(PaymentMessage.REPORT_BY_USER_LOG);
		List<Payment> payments = this.paymentRepository.findAll();
		Map<String, List<Payment>> gropedPaymentsByUser = payments.stream()
				.collect(Collectors.groupingBy(Payment::getUserId));
		List<ReportByUser> reports = new ArrayList<>();
		gropedPaymentsByUser.forEach((userId, paymentsByUser) -> {
			ReportByUser report = new ReportByUser();
			report.setUserId(userId);
			BigDecimal totalAmount = PaymentUtil.calculateTotal(paymentsByUser);
			BigDecimal totalNewAmount = PaymentUtil.calculatePaymentsTotalByStatus(paymentsByUser, PaymentUtil.NEW);
			BigDecimal totalProcessedAmount = PaymentUtil.calculatePaymentsTotalByStatus(paymentsByUser,
					PaymentUtil.PROCESSED);
			report.setPayments(totalAmount);
			report.setNewPayments(totalNewAmount);
			report.setProcessedPayments(totalProcessedAmount);
			reports.add(report);
		});

		return ResponseEntity.ok(reports);

	}
}
