package com.example.clip.service;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.clip.model.User;
import com.example.clip.repository.UserRepository;
import com.example.clip.request.UserRequest;
import com.example.clip.util.enums.UserUtil;

@Service
public class UserService extends SuperService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;

	public ResponseEntity<User> save(UserRequest userRequest) {
		User user = new User();
		user.setUsername(userRequest.getUsername());
		String encryptPassword = UserUtil.encryptPassword(userRequest.getPassword());
		user.setPassword(encryptPassword);
		user.setEmail(userRequest.getEmail());

		User userSaved = userRepository.save(user);

		return new ResponseEntity<User>(userSaved, HttpStatus.CREATED);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);

		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				Collections.emptyList());
	}

}